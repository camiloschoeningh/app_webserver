# Application

**Scenario**

A web app running on port 80 needs to be deployed to a Kubernetes install. For the sake of
this test, please tailor your answer to use a local instance of minikube.
You are tasked with automating the deployment process pipeline, to containerize and deploy
the application to Kubernetes.
Please use the sample Ruby application https://github.com/sawasy/http_server.
It runs on TCP port 80. The application also exposes a health check and endpoint at the route
/healthcheck.

**Requirements**
1. Docker is the tool to support it
2. Ensuring the application is started before served with traffic
3. Enhancing availability with probes
4. Running application as non-root

A README with:
1. step-by-step instructions to recreate the setup
2. the Strategy/Architecture
3. Instructions on how to connect to the running application
4. All the scripts, configs, playbooks, manifest etc should be provided on a DVCs, preferably Github.
5. If your test requires a non-current version of software, please explain why

### Answers

---

#### Assumption/Discussion
To solve the task, assumptions must be made beforehand:

 1. **http_server**
 Patching https://github.com/sawasy/http_server is allowed. This is necessary to make sure that the server is started and can accept requests.
 You can find my patch/branch here: https://github.com/flyingcamilo/http_server/pull/1

 2. **Accessing the http server**
 There are several solutions to access the service from outside. I decided to use the `NodePort access`.
 A NodePort service is the most basic way to get external traffic directly to our service.

 3. **System Requirements**
 The solution was written on the following system and no attention was paid to platform independence:

```bash
└──● uname -a
Linux knecht3 5.16.11-zen1-1-zen #1 ZEN SMP PREEMPT Thu, 24 Feb 2022 02:18:22 +0000 x86_64 GNU/Linux
```

```bash
└──● docker version
client:
 version:           20.10.12
 api version:       1.41
 go version:        go1.17.5
 git commit:        e91ed5707e
 built:             mon dec 13 22:31:40 2021
 os/arch:           linux/amd64
 context:           default
```

```bash
└──● curl -V
curl 7.81.0 (x86_64-pc-linux-gnu) libcurl/7.81.0 OpenSSL/1.1.1m zlib/1.2.11 brotli/1.0.9 zstd/1.5.2 libidn2/2.3.2 libpsl/0.21.1 (+libidn2/2.3.0) libssh2/1.10.0 nghttp2/1.47.0
Release-Date: 2022-01-05
Protocols: dict file ftp ftps gopher gophers http https imap imaps mqtt pop3 pop3s rtsp scp sftp smb smbs smtp smtps telnet tftp
Features: alt-svc AsynchDNS brotli GSS-API HSTS HTTP2 HTTPS-proxy IDN IPv6 Kerberos Largefile libz NTLM NTLM_WB PSL SPNEGO SSL TLS-SRP UnixSockets zstd
```

```bash
└──● minikube version
minikube version: v1.25.1
commit: 3e64b11ed75e56e4898ea85f96b2e4af0301f43d-dirty
```

---

### 1 Demo
#### 1.1 Installation
For simplicity I have created a Makefile. The command `make all` does the following steps:

1. it builds the image for the http server, which will be used in k8s
2. it builds an image which will be used as a "deployer" image. On this image are installed kubectl and helm
3. it starts minikube with two nodes. After that the built `http server` image is pushed into the cluster
4. via the "deployer" image the http server is installed as helm chart

After the last step an output is given, which can be used to access the http service.

```bash
# Help
foo@bar:~$ make help
all                            This is bringing up the http server demo.
clean                          This will delete the minikube cluster
build-docker-images            Build docker containers
build-docker-images-no-cache   Build docker containers without using the cache
start-minikube                 Starts minikube
stop-minikube                  Stops minikube
remove-minikube                Removes the minikube cluster (only the application profile)
remove-docker-images           Removes all docker images.
push-images-to-minikube        Push docker images to minikube
helm-bash                      Runs a bash inside the helm/kubectl container
install-http-server            Installs the http server inside the minikube cluster
uninstall-http-server          Uninstalls the http server.
update-http-server             Update the http server.
```

```bash
# Start the cluster and webserver
foo@bar:~$ make all
```

#### 1.2 Test connection

```bash
└──● echo $(curl -v -k $(minikube service --profile=adjust-camilo --namespace adjust --url http-server))
*   Trying 192.168.49.2:30207...
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0* Connected to 192.168.49.2 (192.168.49.2) port 30207 (#0)
> GET / HTTP/1.1
> Host: 192.168.49.2:30207
> User-Agent: curl/7.81.0
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Date: 2022-03-01 11:25:41 +0000
< Content-Length: 18
< Content-Type: text/html; charset=iso-8859-1
<
{ [18 bytes data]
100    18  100    18    0     0  39045      0 --:--:-- --:--:-- --:--:-- 18000
* Connection #0 to host 192.168.49.2 left intact
Well, hello there!
```

#### 1.3 High availability

High availability is enabled. We use the horizontal pod autoscaler with replica_min set to 2 and replica_max to 10 and
we use the memory consumption as a metric for scaling the cluster (the cpu metric was not working in the time of writing).
However if needed, the scaling can be testes as we have three nodes up.

```bash
└──● kubectl -n adjust get horizontalpodautoscalers.autoscaling
NAME          REFERENCE                TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
http-server   Deployment/http-server   77%/80%   2         10        2          6m9s
```

**Liveness/Readiness probes**
Liveness probes and readiness probes have been enabled. This is to make sure, a pod is only getting traffic when the pod is up
and initialized.

```helm
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /healthcheck
              port: http
            initialDelaySeconds: 3
            periodSeconds: 3
          readinessProbe:
            httpGet:
              path: /
              port: http
            initialDelaySeconds: 5
            periodSeconds: 10
```

**podantiaffinity**

We have created a podantiaffinity rule that ensures that only one webserver is ever deployed on a node.

```helm
affinity:
  podAntiAffinity:
    requiredDuringSchedulingIgnoredDuringExecution:
    - labelSelector:
        matchExpressions:
        - key: app.kubernetes.io/name
          operator: In
          values:
          - http-server
      topologyKey: "kubernetes.io/hostname"
```

```bash
└──☻ kubectl -n adjust get pods -o wide
NAME                           READY   STATUS    RESTARTS   AGE    IP           NODE
http-server-68fcc98cbf-7wps4   1/1     Running   0          8m9s   10.244.1.7   adjust-camilo-m02
http-server-68fcc98cbf-ljwz6   1/1     Running   0          8m9s   10.244.0.9   adjust-camilo
```

#### 1.4 No root

I was used to creating a podsecurity police to make sure that pods don't get rights they shouldn't have.
However, podsecuritypolices are provided with the flag deprectated. So they will be gone in the new k8s versions. We therefore use the podSecurity and the securityContext to prevent root. In fact a user with ID=0 will be prevented.

```helm
podSecurityContext:
  runAsUser: 1000
  runAsGroup: 3000
  fsGroup: 2000

securityContext:
  capabilities:
    drop:
    - ALL
  allowPrivilegeEscalation: false
  readOnlyRootFilesystem: true
  runAsNonRoot: true
  runAsUser: 1000
```

```bash
# id of the default user
└──● kubectl -n adjust exec -it deploy/http-server -- id
uid=1000(adjust) gid=3000(adjust) groups=2000

# su is not possible
└──● kubectl -n adjust exec -it deploy/http-server -- su -
su: must be suid to work properly
command terminated with exit code 1

# Server running with the correct user
└──● kubectl -n adjust exec -it deploy/http-server -- ps aux
USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
adjust         1  0.0  0.0  73212 13288 ?        Ss   11:13   0:00 ruby /http_se

```
---
