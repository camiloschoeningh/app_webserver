MINIKUBE_PROFILE=adjust-camilo

SHELL := /bin/bash
PRINT_START = \e[0;33m
PRINT_END=$<\e[0m

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

# DOCKER TASKS
# Helm container
KUBECONFIG ?= ${HOME}/.kube/config
define HELM_CMD
docker run -it --rm \
	-v $(PWD):/src \
	-v $(KUBECONFIG):/root/.kube/config \
	-e KUBECONFIG=/root/.kube/config \
	--network host \
	deployer:${MINIKUBE_PROFILE}
endef

all: build-docker-images start-minikube push-images-to-minikube install-http-server ## This is bringing up the http server demo.
clean: remove-minikube remove-docker-images ## This will delete the minikube cluster and the docker images.

.PHONY: build-docker-images
build-docker-images: ## Build docker containers
	@echo -e "$(PRINT_START)*** Building images ***$(PRINT_END)"
	@docker build -t http_server:${MINIKUBE_PROFILE} - < docker/http_server.Dockerfile
	@docker build -t deployer:${MINIKUBE_PROFILE} - < docker/deployer.Dockerfile

.PHONY: build-docker-images-no-cache
build-docker-images-no-cache: ## Build docker containers without using the cache
	@echo -e "$(PRINT_START)*** Building images without using the docker cache ***$(PRINT_END)"
	@docker build -t http_server:${MINIKUBE_PROFILE} - < docker/http_server.Dockerfile --no-cache
	@docker build -t deployer:${MINIKUBE_PROFILE} - < docker/deployer.Dockerfile --no-cache

.PHONY: start-minikube
start-minikube: ## Starts minikube
	@echo -e "$(PRINT_START)*** Starting minikube ***$(PRINT_END)"
	scripts/minikube.sh start ${MINIKUBE_PROFILE}

.PHONY: stop-minikube
stop-minikube: ## Stops minikube
	@echo -e "$(PRINT_START)*** Stopping minikube ***$(PRINT_END)"
	scripts/minikube.sh stop ${MINIKUBE_PROFILE}

.PHONY: remove-minikube
remove-minikube: ## Removes the minikube cluster (only the application profile)
	@echo -e "$(PRINT_START)*** Removing minikube cluster ${MINIKUBE_PROFILE} ***$(PRINT_END)"
	@scripts/minikube.sh status ${MINIKUBE_PROFILE} > /dev/null 2>&1 && scripts/minikube.sh stop ${MINIKUBE_PROFILE} || true
	minikube delete --profile ${MINIKUBE_PROFILE}

.PHONY: remove-docker-images
remove-docker-images: ## Removes all docker images.
	@echo -e "$(PRINT_START)*** Removing docker images ***$(PRINT_END)"
	@docker rmi -f $(shell docker images --format "{{.Repository}}:{{.Tag}}" | grep ${MINIKUBE_PROFILE}) 2>/dev/null || true

.PHONY: push-images-to-minikube
push-images-to-minikube: ## Push docker images to minikube
	@echo -e "$(PRINT_START)*** Pushing images to minikube ***$(PRINT_END)"
	@if [ -z "$(shell docker images -q http_server:${MINIKUBE_PROFILE} 2> /dev/null)" ]; then \
            (echo "[IMAGE] Dockerfile http_server is not build yet. Please run make build-docker-images.";exit 1); \
        fi

	@minikube --profile ${MINIKUBE_PROFILE} image load http_server:${MINIKUBE_PROFILE}
	@echo "Done."

helm-bash: ## Runs a bash inside the helm/kubectl container
	$(HELM_CMD) /bin/bash

.PHONY: install-http-server
install-http-server: ## Installs the http server inside the minikube cluster
	$(eval MODE := $(shell helm status -n adjust http-server 2>/dev/null | grep 'STATUS: deployed' > /dev/null && echo upgrade || echo install))
	@echo -e "$(PRINT_START)*** Installing ruby http server to Namespace adjust ***$(PRINT_END)"
	@if [ -z "$(shell docker images -q deployer:${MINIKUBE_PROFILE} 2> /dev/null)" ]; then \
            (echo "[IMAGE] Dockerfile deployer is not build yet. Please run make build-docker-images.";exit 1); \
        fi
	@if [ "${MODE}" = "install" ]; then \
		$(HELM_CMD) /usr/local/bin/helm install --create-namespace --set image.tag=${MINIKUBE_PROFILE} --wait -n adjust http-server /src/helm/http-server ;\
	else \
		$(HELM_CMD) /usr/local/bin/helm upgrade --set image.tag=${MINIKUBE_PROFILE} --wait -n adjust http-server /src/helm/http-server ;\
	fi

	@minikube service --profile=${MINIKUBE_PROFILE} --namespace adjust --url http-server

.PHONY: uninstall-http-server
uninstall-http-server: ## Uninstalls the http server.
	@echo -e "$(PRINT_START)*** Removing ruby http server ***$(PRINT_END)"
	@$(HELM_CMD) /usr/local/bin/helm uninstall -n adjust http-server || true
	@echo -e "$(PRINT_START)*** Removing minikube cluster ${MINIKUBE_PROFILE} ***$(PRINT_END)"

.PHONY: update-http-server
update-http-server: ## Update the http server.
	@echo -e "$(PRINT_START)*** Updating http server deployment ***$(PRINT_END)"
	@$(HELM_CMD) /usr/local/bin/helm upgrade \
		--set image.tag=${MINIKUBE_PROFILE} \
		--wait \
		-n adjust \
		http-server /src/helm/http-server
	@minikube service --profile=${MINIKUBE_PROFILE} --namespace adjust --url http-server
