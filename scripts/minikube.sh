#!/bin/bash

PROFILE="${2:-adjust-camilo}"

# This status function is not meant to be stable. We don't catch the case were minikube gives us
# an error back.
function status() {
  status=$(minikube --profile=${PROFILE} status -o json | jq '.[0] | select(.Host=="Running" and .Kubelet=="Running" and .APIServer=="Running")')
  if [[ ${#status} -gt 0 ]]; then
    echo "Running"
    exit 0
  else
    echo "Not running.."
    exit 1
  fi
}

# start() will startup minikube with a profile applied.
function start() {
  minikube start \
    --addons metrics-server \
    --container-runtime=docker \
    --embed-certs \
    --kubernetes-version "v1.23.3" \
    --nodes 3 \
    --profile ${PROFILE}
}

# stop() will stop the minikube cluster with a profile applied.
function stop() {
  minikube stop \
    --profile ${PROFILE}
}

case "$1" in
start) start ;;
status) status ;;
stop) stop ;;
*)
  echo "usage: $0 start|stop|status" >&2
  exit 1
  ;;
esac
