#**********************************************************************************************/
#*The ruby webserver:                                                                         */
#*1. The webserver had to be patched because it did not provide a HEADER and HTTP status code,*/
#*   and thus was not usable for the kubernetes liveness/readiness probe                      */
#**********************************************************************************************/

FROM ruby:3.1-alpine3.15

RUN apk add --no-cache curl bash

# Create User
RUN addgroup -g 3000 adjust \
      && adduser -u 1000 -G adjust --disabled-password -s /bin/bash adjust

RUN export DST=/tmp/http_server \
	&& mkdir -p $DST $DST/extracted /http_server \
	&& curl -L -o $DST/server.zip "https://github.com/flyingcamilo/http_server/archive/refs/heads/response_code.zip" \
	&& unzip -j $DST/server.zip -d $DST/extracted \
  && find $DST/extracted -type f -exec install --owner="adjust" --group="adjust" "{}" "/http_server" \; \
	&& rm -rf $DST

USER adjust
